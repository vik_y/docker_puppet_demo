
node default {
  notify {'default-node':
    message => "I am new node (Environment: ${::environment}, Hostname: ${::hostname}). Add a node definition, duh!",
    loglevel => info
  }

  # Install apache2
  package {'apache2':
    ensure => present
  }

  # Ensure apache2 is running
  service {'apache2':
    ensure => running,
    enable => true,
    require => Package['apache2']
  }

  # Install apache2
  package {'vim':
    ensure => present
  }
}

node host1 { 
   notify {'default-node':
    message => "I am new node (Environment: ${::environment}, Hostname: ${::hostname}). Add a node definition, duh!",
    loglevel => info
  }

  # Install apache2
  package {'nginx':
    ensure => present
  }

  # Ensure apache2 is running
  service {'nginx':
    ensure => running,
    enable => true,
    require => Package['nginx']
  }
}

node host2 { 
   notify {'default-node':
    message => "I am new node (Environment: ${::environment}, Hostname: ${::hostname}). Add a node definition, duh!",
    loglevel => info
  }

  # Install apache2
  package {'vim':
    ensure => present
  }
}

node host3 { 
   notify {'default-node':
    message => "I am new node (Environment: ${::environment}, Hostname: ${::hostname}). Add a node definition, duh!",
    loglevel => info
  }

  # Install apache2
  file {'/root/by-puppet':
    ensure => present,
    content => 'Hi, I came from puppet'
  }
}
