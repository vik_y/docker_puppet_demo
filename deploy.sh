#!/bin/bash 

# Build everything that's required
./build.sh

# Run puppet server with hostname puppet
docker run -d -h puppet -v $(pwd)/code:/etc/puppetlabs/code --name puppet_server puppet/puppetserver 

# Note here that I am runnning puppetserver with -h puppet flag. It is very very important
# step because the ssl certificate generation and verification are dependent on the hostname
# so hostname o the puppetserver has to match with server mentioned in puppet_client/conf/puppet.conf


