# Introduction
We have a basic puppet code which we are going to use along with our puppetserver. We will have a puppet slave 
which will also run inside a docker container. 

The end result is to run the pupppet server and make the client node pull configuration from puppet server.


## Initializing puppet server 

For this example, I am going to use the official puppetserver image provided by puppet. 
https://hub.docker.com/r/puppet/puppetserver/

The code used to build this image is available here, you can refer to it: 
ttps://github.com/puppetlabs/puppetserver/tree/master/docker/puppetserver-standalone

## Test environment 

In production scenario you won't run puppet-agent inside a container. Just for demonstration I am 
installing puppet-agent inside docker container so that I can demonstrate to you a working setup 

-> Puppet server container (hostname - puppet-server) 
-> Puppet agent container -> to be spawned for testing


## Execution

Run the deploy script which will build the puppet_client image and also deploy the server `./deploy.sh`

Check if the puppet server is healthy or not - it takes about 10-30 seconds for it to be working. Use `docker ps` 
for it. 

Once puppet server is healthy run: 

`docker run -it --link puppet_server:puppet --rm puppet_client bash` 

Please lookup about link in docker. Basically in `puppet_server:puppet` we are adding a dns entry in docker that hostname puppet is reachable
at the ip address of container named puppet_server 

I hope you are aware of basic puppet commands. Run this command to pull configuration from server 
`puppet agent -tv` 
